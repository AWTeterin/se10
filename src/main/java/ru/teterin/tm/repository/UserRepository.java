package ru.teterin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.repository.IUserRepository;
import ru.teterin.tm.entity.User;

import java.util.Collection;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        @NotNull final Collection<User> users = entities.values();
        for (@NotNull final User user : users) {
            @Nullable final String userLogin = user.getName();
            if (userLogin != null && userLogin.equals(login)) {
                return user;
            }
        }
        return null;
    }

}
