package ru.teterin.tm.context;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.repository.IProjectRepository;
import ru.teterin.tm.api.repository.ITaskRepository;
import ru.teterin.tm.api.repository.IUserRepository;
import ru.teterin.tm.api.service.*;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.entity.User;
import ru.teterin.tm.enumerated.Role;
import ru.teterin.tm.exception.*;
import ru.teterin.tm.repository.ProjectRepository;
import ru.teterin.tm.repository.TaskRepository;
import ru.teterin.tm.repository.UserRepository;
import ru.teterin.tm.service.*;
import ru.teterin.tm.util.HashUtil;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.Set;

@Getter
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository, projectRepository);

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository, taskRepository);

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final IStateService stateService = new StateService();

    @NotNull
    private final ITerminalService terminalService = new TerminalService();

    @NotNull
    private final IDomainService domainService = new DomainService(userService, projectService, taskService);

    public void init() {
        initUser();
        initCommands();
        terminalService.print(Constant.START_MESSAGE);
        while (true) {
            @Nullable final String cmd = terminalService.readString();
            try {
                @NotNull final AbstractCommand command = stateService.getCommand(cmd);
                @NotNull final Role[] roles = command.roles();
                if (command.secure() && deniedAccess(roles)) {
                    terminalService.print(Constant.INCORRECT_COMMAND);
                    continue;
                }
                command.execute();
            } catch (
                ObjectExistException
                    | IllegalArgumentException
                    | ObjectNotFoundException
                    | IOException
                    | JAXBException e
            ) {
                terminalService.print(e.getMessage());
            }
        }
    }

    private void initCommands() {
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
            new Reflections(Constant.ROOT_PACKAGE).getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class clazz : classes) {
            try {
                @Nullable final Object object = clazz.newInstance();
                if (object instanceof AbstractCommand) {
                    @NotNull final AbstractCommand command = (AbstractCommand) object;
                    command.setServiceLocator(this);
                    command.setTerminalService(terminalService);
                    stateService.registryCommand(command);
                }
            } catch (InstantiationException | IllegalAccessException e) {
                throw new IllegalArgumentException(Constant.INVALID_COMMAND);
            }
        }
    }

    private void initUser() {
        @NotNull final User user = new User();
        user.setId("00000000-0000-0000-0000-000000000100");
        user.setUserId("00000000-0000-0000-0000-000000000100");
        user.setName("user");
        user.setPassword(HashUtil.md5Custom("user"));
        user.setRole(Role.USER);
        @NotNull final User admin = new User();
        admin.setId("00000000-0000-0000-0000-000000000111");
        admin.setUserId("00000000-0000-0000-0000-000000000111");
        admin.setName("admin");
        admin.setPassword(HashUtil.md5Custom("admin"));
        admin.setRole(Role.ADMIN);
        userRepository.persist(user.getId(), user);
        userRepository.persist(admin.getId(), admin);
    }

    private boolean deniedAccess(@NotNull final Role[] roles) {
        @Nullable final Role userRole = stateService.getRole();
        for (@NotNull Role role : roles) {
            if (userRole == role) {
                return false;
            }
        }
        return true;
    }

}
