package ru.teterin.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.constant.Constant;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class HashUtil {

    @NotNull
    public static String md5Custom(@Nullable final String st) {
        if (st == null || st.isEmpty()) {
            throw new IllegalArgumentException(Constant.NO_PASSWORD);
        }
        @NotNull byte[] digest = new byte[0];
        try {
            @NotNull final MessageDigest messageDigest = MessageDigest.getInstance(Constant.MD5);
            messageDigest.reset();
            messageDigest.update(st.getBytes());
            digest = messageDigest.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        @NotNull final BigInteger bigInt = new BigInteger(1, digest);
        @NotNull final StringBuilder md5Hex = new StringBuilder(bigInt.toString(16));
        while (md5Hex.length() < 32)
            md5Hex.insert(0, "0");
        return md5Hex.toString();
    }

}
