package ru.teterin.tm.api.context;

import org.jetbrains.annotations.NotNull;
import ru.teterin.tm.api.service.*;

public interface IServiceLocator {

    @NotNull
    public IProjectService getProjectService();

    @NotNull
    public ITaskService getTaskService();

    @NotNull
    public IUserService getUserService();

    @NotNull
    public IStateService getStateService();

    @NotNull
    public IDomainService getDomainService();

}
