package ru.teterin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.entity.Project;
import ru.teterin.tm.entity.Task;

import java.util.Collection;
import java.util.Comparator;

public interface IProjectService extends IService<Project> {

    @NotNull
    public Project checkProject(
        @Nullable final String userId,
        @NotNull final Project project
    );

    @NotNull
    public Collection<Task> findAllTaskByProjectId(
        @Nullable final String userId,
        @Nullable final String id
    );

    @NotNull
    public Collection<Project> findAndSortAll(
        @Nullable final String userId,
        @Nullable String sortOption
    );

    @NotNull
    public Comparator<Project> createComparator(@NotNull final String sortOption);

    @NotNull
    public Collection<Project> searchByString(
        @Nullable final String userId,
        @Nullable final String searchString
    );

}
