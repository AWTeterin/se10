package ru.teterin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.entity.AbstractEntity;

import java.util.List;

public interface IService<T extends AbstractEntity> {

    @NotNull
    public T findOne(
        @Nullable final String userId,
        @Nullable final String id
    );

    @NotNull
    public List<T> findAll();

    @NotNull
    public List<T> findAll(@Nullable final String userId);

    @Nullable
    public T persist(@Nullable final T t);

    @Nullable
    public T persist(
        @Nullable final String userId,
        @Nullable final T t
    );

    @Nullable
    public T merge(
        @Nullable final String userId,
        @Nullable final T t
    );

    @NotNull
    public T remove(
        @Nullable final String userId,
        @Nullable final String id
    );

    public void removeAll();

    public void removeAll(@Nullable final String userId);

}
