package ru.teterin.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.entity.Domain;

public interface IDomainService {

    public void save(@Nullable final Domain domain);

    public void load(@Nullable final Domain domain);

}
