package ru.teterin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.entity.Task;

import java.util.Collection;
import java.util.Comparator;

public interface ITaskService extends IService<Task> {

    @NotNull
    public Task checkTask(
        @Nullable final String userId,
        @NotNull final Task task
    );

    @NotNull
    public Task linkTask(
        @Nullable final String userId,
        @Nullable final String projectId,
        @Nullable final String id
    );

    @NotNull
    public Collection<Task> findAndSortAll(
        @Nullable final String userId,
        @Nullable final String sortOption
    );

    @NotNull
    public Comparator<Task> createComparator(@NotNull final String sortOption);

    @NotNull
    public Collection<Task> searchByString(
        @Nullable final String userId,
        @Nullable final String searchString
    );

}
