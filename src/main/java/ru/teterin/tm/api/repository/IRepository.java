package ru.teterin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.entity.AbstractEntity;

import java.util.List;

public interface IRepository<T extends AbstractEntity> {

    @Nullable
    public T findOne(@NotNull final String id);

    @Nullable
    public T findOne(
        @NotNull final String userId,
        @NotNull final String id
    );

    @NotNull
    public List<T> findAll();

    @NotNull
    public List<T> findAll(@NotNull final String userId);

    @Nullable
    public T persist(@NotNull final T t);

    @Nullable
    public T persist(
        @NotNull final String userId,
        @NotNull final T t
    );

    @Nullable
    public T merge(
        @NotNull final String userId,
        @NotNull final T t
    );

    @Nullable
    public T remove(
        @NotNull final String userId,
        @NotNull final String id
    );

    public void removeAll();

    public void removeAll(@NotNull final String userId);

}
