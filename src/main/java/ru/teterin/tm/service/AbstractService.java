package ru.teterin.tm.service;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.repository.IRepository;
import ru.teterin.tm.api.service.IService;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.entity.AbstractEntity;
import ru.teterin.tm.exception.ObjectNotFoundException;
import ru.teterin.tm.util.DateUuidParseUtil;

import java.util.List;

@AllArgsConstructor
public abstract class AbstractService<T extends AbstractEntity> implements IService<T> {

    @NotNull
    private final IRepository<T> repository;

    @NotNull
    @Override
    public List<T> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public T findOne(
        @Nullable final String userId,
        @Nullable final String id
    ) {
        if (id == null || id.isEmpty() || userId == null || userId.isEmpty()) {
            throw new IllegalArgumentException(Constant.EMPTY_ID);
        }
        @Nullable final T t = repository.findOne(DateUuidParseUtil.isUuid(userId), DateUuidParseUtil.isUuid(id));
        if (t == null) {
            throw new ObjectNotFoundException();
        }
        return t;
    }

    @NotNull
    @Override
    public List<T> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) {
            throw new IllegalArgumentException(Constant.EMPTY_ID);
        }
        return repository.findAll(DateUuidParseUtil.isUuid(userId));
    }

    @Nullable
    @Override
    public T persist(@Nullable final T t) {
        if (t == null) {
            throw new IllegalArgumentException(Constant.EMPTY_OBJECT);
        }
        @Nullable final String name = t.getName();
        @Nullable final String tUserId = t.getUserId();
        final boolean emptyT = name == null || name.isEmpty() || tUserId == null;
        if (emptyT) {
            throw new IllegalArgumentException(Constant.EMPTY_OBJECT);
        }
        @Nullable final T result = repository.persist(t);
        return result;
    }

    @Nullable
    @Override
    public T persist(
        @Nullable final String userId,
        @Nullable final T t
    ) {
        if (userId == null || userId.isEmpty()) {
            throw new IllegalArgumentException(Constant.EMPTY_ID);
        }
        if (t == null) {
            throw new IllegalArgumentException(Constant.EMPTY_OBJECT);
        }
        @Nullable final String name = t.getName();
        @Nullable final String tUserId = t.getUserId();
        final boolean emptyT = name == null || name.isEmpty() || tUserId == null;
        if (emptyT) {
            throw new IllegalArgumentException(Constant.EMPTY_OBJECT);
        }
        final boolean incorrectUserId = !userId.equals(tUserId);
        if (incorrectUserId) {
            throw new IllegalArgumentException(Constant.ERROR_OBJECT_ACCESS);
        }
        @Nullable final T result = repository.persist(DateUuidParseUtil.isUuid(userId), t);
        return result;
    }

    @Nullable
    @Override
    public T merge(
        @Nullable final String userId,
        @Nullable final T t
    ) {
        if (userId == null || userId.isEmpty()) {
            throw new IllegalArgumentException(Constant.EMPTY_ID);
        }
        if (t == null) {
            throw new IllegalArgumentException(Constant.EMPTY_OBJECT);
        }
        @Nullable final String name = t.getName();
        @Nullable final String tUserId = t.getUserId();
        final boolean emptyT = name == null || name.isEmpty() || tUserId == null;
        if (emptyT) {
            throw new IllegalArgumentException(Constant.EMPTY_OBJECT);
        }
        final boolean incorrectUserId = !userId.equals(tUserId);
        if (incorrectUserId) {
            throw new IllegalArgumentException(Constant.ERROR_OBJECT_ACCESS);
        }
        return repository.merge(DateUuidParseUtil.isUuid(userId), t);
    }

    @NotNull
    @Override
    public T remove(
        @Nullable final String userId,
        @Nullable final String id
    ) {
        if (id == null || id.isEmpty() || userId == null || userId.isEmpty()) {
            throw new IllegalArgumentException(Constant.EMPTY_ID);
        }
        @Nullable final T t = repository.remove(DateUuidParseUtil.isUuid(userId), DateUuidParseUtil.isUuid(id));
        if (t == null) {
            throw new ObjectNotFoundException();
        }
        return t;
    }

    @Override
    public void removeAll() {
        repository.removeAll();
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) {
            throw new IllegalArgumentException(Constant.EMPTY_ID);
        }
        repository.removeAll(DateUuidParseUtil.isUuid(userId));
    }

}
