package ru.teterin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.repository.IUserRepository;
import ru.teterin.tm.api.service.IUserService;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.entity.User;
import ru.teterin.tm.exception.ObjectExistException;
import ru.teterin.tm.exception.ObjectNotFoundException;
import ru.teterin.tm.util.HashUtil;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    public UserService(@NotNull final IUserRepository repository) {
        super(repository);
        this.userRepository = repository;
    }

    @NotNull
    @Override
    public User create(
        @NotNull final User user
    ) {
        @Nullable final String login = user.getName();
        final boolean noLogin = login == null || login.isEmpty();
        @Nullable final String password = user.getPassword();
        final boolean noPassword = password == null || password.isEmpty();
        if (noLogin || noPassword) {
            throw new IllegalArgumentException(Constant.EMPTY_OBJECT);
        }
        @NotNull final String hashPassword = HashUtil.md5Custom(password);
        @Nullable final User userFromRepository = userRepository.findByLogin(login);
        if (userFromRepository != null) {
            throw new ObjectExistException();
        }
        user.setPassword(hashPassword);
        return user;
    }

    @NotNull
    @Override
    public User checkUser(
        @Nullable final String login,
        @Nullable final String password
    ) {
        final boolean incorrectLogin = login == null || login.isEmpty();
        final boolean incorrectPassword = password == null || password.isEmpty();
        if (incorrectLogin || incorrectPassword) {
            throw new IllegalArgumentException(Constant.EMPTY_OBJECT);
        }
        @Nullable final User user = userRepository.findByLogin(login);
        @NotNull final String hashPassword = HashUtil.md5Custom(password);
        if (user == null) {
            throw new ObjectNotFoundException(Constant.USER_NOT_FOUND);
        }
        @Nullable final String userPassword = user.getPassword();
        final boolean invalidPassword = !(hashPassword.equals(userPassword));
        if (invalidPassword) {
            throw new IllegalArgumentException(Constant.INCORRECT_PASSWORD);
        }
        return user;
    }

    @Override
    public void changePassword(
        @Nullable final String login,
        @Nullable final String oldPassword,
        @Nullable final String newPassword
    ) {
        final boolean noLogin = login == null || login.isEmpty();
        final boolean noPassword = (oldPassword == null || oldPassword.isEmpty())
            || (newPassword == null || newPassword.isEmpty());
        if (noLogin || noPassword) {
            throw new IllegalArgumentException(Constant.EMPTY_OBJECT);
        }
        @NotNull final User user = checkUser(login, oldPassword);
        user.setPassword(HashUtil.md5Custom(newPassword));
    }

    @Override
    public void editUser(
        @Nullable final String oldLogin,
        @Nullable final String newLogin
    ) {
        final boolean noOldLogin = oldLogin == null || oldLogin.isEmpty();
        final boolean noNewLogin = newLogin == null || newLogin.isEmpty();
        if (noOldLogin || noNewLogin) {
            throw new IllegalArgumentException(Constant.EMPTY_OBJECT);
        }
        final boolean loginIsBusy = userRepository.findByLogin(newLogin) != null;
        if (loginIsBusy) {
            throw new ObjectExistException();
        }
        @Nullable final User user = userRepository.findByLogin(oldLogin);
        if (user != null) {
            user.setName(newLogin);
        }
    }

}
