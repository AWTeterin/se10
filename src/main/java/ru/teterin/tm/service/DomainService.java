package ru.teterin.tm.service;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.service.IDomainService;
import ru.teterin.tm.api.service.IProjectService;
import ru.teterin.tm.api.service.ITaskService;
import ru.teterin.tm.api.service.IUserService;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.entity.Domain;
import ru.teterin.tm.entity.Project;
import ru.teterin.tm.entity.Task;
import ru.teterin.tm.entity.User;

import java.util.Collection;

@AllArgsConstructor
public final class DomainService implements IDomainService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskService taskService;

    @Override
    public void save(@Nullable Domain domain) {
        if (domain == null) {
            throw new IllegalArgumentException(Constant.NO_DOMAIN);
        }
        domain.setUsers(userService.findAll());
        domain.setProjects(projectService.findAll());
        domain.setTasks(taskService.findAll());
    }

    @Override
    public void load(@Nullable Domain domain) {
        if (domain == null) {
            throw new IllegalArgumentException(Constant.NO_DOMAIN);
        }
        @Nullable final Collection<User> users = domain.getUsers();
        if (users == null) {
            throw new IllegalArgumentException(Constant.OBJECT_NOT_FOUND);
        }
        for (@NotNull User user : users) {
            userService.persist(user);
        }
        @Nullable final Collection<Project> projects = domain.getProjects();
        if (projects == null) {
            throw new IllegalArgumentException(Constant.NO_PROJECT);
        }
        for (@NotNull Project project : projects) {
            projectService.persist(project);
        }
        @Nullable final Collection<Task> tasks = domain.getTasks();
        if (tasks == null) {
            throw new IllegalArgumentException(Constant.NO_TASK);
        }
        for (@NotNull Task task : tasks) {
            taskService.persist(task);
        }
    }

}
