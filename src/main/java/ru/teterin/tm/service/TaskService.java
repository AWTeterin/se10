package ru.teterin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.repository.IProjectRepository;
import ru.teterin.tm.api.repository.ITaskRepository;
import ru.teterin.tm.api.service.ITaskService;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.entity.Project;
import ru.teterin.tm.entity.Task;
import ru.teterin.tm.exception.*;
import ru.teterin.tm.util.DateUuidParseUtil;

import java.util.Collection;
import java.util.Comparator;
import java.util.Date;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final ITaskRepository taskRepository;

    public TaskService(
        @NotNull final ITaskRepository taskRepository,
        @NotNull final IProjectRepository projectRepository
    ) {
        super(taskRepository);
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @NotNull
    @Override
    public Task checkTask(
        @Nullable final String userId,
        @NotNull final Task task
    ) {
        @Nullable final String projectId = task.getProjectId();
        if (userId == null || userId.isEmpty() || projectId == null || projectId.isEmpty()) {
            throw new IllegalArgumentException(Constant.EMPTY_ID);
        }
        @Nullable final String name = task.getName();
        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException(Constant.EMPTY_OBJECT);
        }
        @Nullable final String description = task.getDescription();
        if (description == null) {
            throw new IllegalArgumentException(Constant.EMPTY_OBJECT);
        }
        task.setUserId(DateUuidParseUtil.isUuid(userId));
        task.setProjectId(DateUuidParseUtil.isUuid(projectId));
        return task;
    }

    @NotNull
    @Override
    public Task linkTask(
        @Nullable final String userId,
        @Nullable final String projectId,
        @Nullable final String id
    ) {
        final boolean noProjectId = projectId == null || projectId.isEmpty();
        final boolean noTaskId = id == null || id.isEmpty();
        final boolean noUserId = userId == null || userId.isEmpty();
        if (noProjectId || noTaskId || noUserId) {
            throw new IllegalArgumentException(Constant.EMPTY_ID);
        }
        @Nullable final Project project = projectRepository.findOne(DateUuidParseUtil.isUuid(userId),
            DateUuidParseUtil.isUuid(projectId));
        @Nullable final Task task = findOne(userId, DateUuidParseUtil.isUuid(id));
        if (project == null) {
            throw new ObjectNotFoundException();
        }
        task.setProjectId(projectId);
        return task;
    }

    @NotNull
    @Override
    public Collection<Task> findAndSortAll(
        @Nullable final String userId,
        @Nullable String sortOption
    ) {
        if (userId == null || userId.isEmpty()) {
            throw new IllegalArgumentException(Constant.EMPTY_ID);
        }
        DateUuidParseUtil.isUuid(userId);
        if (sortOption == null) {
            sortOption = Constant.CREATE_ASC_SORT;
        }
        @NotNull final Comparator<Task> comparator = createComparator(sortOption);
        @NotNull final Collection<Task> tasks = taskRepository.findAndSortAll(userId, comparator);
        return tasks;
    }

    @NotNull
    @Override
    public Comparator<Task> createComparator(@NotNull final String sortOption) {
        @NotNull final Comparator<Task> comparator = (Task first, Task second) -> {
            switch (sortOption) {
                default:
                    @NotNull Date firstCreateDate = first.getDateCreate();
                    @NotNull Date secondCreateDate = second.getDateCreate();
                    return firstCreateDate.compareTo(secondCreateDate);
                case Constant.CREATE_DESC_SORT:
                    firstCreateDate = first.getDateCreate();
                    secondCreateDate = second.getDateCreate();
                    return secondCreateDate.compareTo(firstCreateDate);
                case Constant.START_DATE_ASC_SORT:
                    @NotNull Date firstStartDate = first.getDateStart();
                    @NotNull Date secondStartDate = second.getDateStart();
                    return firstStartDate.compareTo(secondStartDate);
                case Constant.START_DATE_DESC_SORT:
                    firstStartDate = first.getDateStart();
                    secondStartDate = second.getDateStart();
                    return secondStartDate.compareTo(firstStartDate);
                case Constant.END_DATE_ASC_SORT:
                    @NotNull Date firstEndDate = first.getDateEnd();
                    @NotNull Date secondEndDate = second.getDateEnd();
                    return firstEndDate.compareTo(secondEndDate);
                case Constant.END_DATE_DESC_SORT:
                    firstEndDate = first.getDateEnd();
                    secondEndDate = second.getDateEnd();
                    return secondEndDate.compareTo(firstEndDate);
                case Constant.STATUS_ASC_SORT:
                    return first.getStatus().compareTo(second.getStatus());
                case Constant.STATUS_DESC_SORT:
                    return second.getStatus().compareTo(first.getStatus());
            }
        };
        return comparator;
    }

    @NotNull
    @Override
    public Collection<Task> searchByString(
        @Nullable final String userId,
        @Nullable String searchString
    ) {
        if (userId == null || userId.isEmpty()) {
            throw new IllegalArgumentException(Constant.EMPTY_ID);
        }
        DateUuidParseUtil.isUuid(userId);
        if (searchString == null) {
            searchString = "";
        }
        @NotNull final Collection<Task> tasks = taskRepository.searchByString(userId, searchString);
        return tasks;
    }

}
