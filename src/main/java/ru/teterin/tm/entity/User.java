package ru.teterin.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.enumerated.Role;

import java.io.Serializable;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public final class User extends AbstractEntity implements Serializable {

    public static final long serialVersionUID = 3;

    @Nullable
    private String password;

    @Nullable
    private Role role = Role.USER;

}
