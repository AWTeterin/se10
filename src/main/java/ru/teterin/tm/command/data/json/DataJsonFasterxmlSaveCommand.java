package ru.teterin.tm.command.data.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.jetbrains.annotations.NotNull;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.entity.Domain;
import ru.teterin.tm.enumerated.Role;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public final class DataJsonFasterxmlSaveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "data-json-fx-save";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "The save of the subject area in the transport format JSON with the use FasterXML.";
    }

    @Override
    public void execute() throws IOException {
        terminalService.print(Constant.DATA_JSON_FX_SAVE);
        @NotNull final String fileName = Constant.DIRECTORY + "\\" + Constant.JSON_FILE_NAME;
        @NotNull final Path path = Paths.get(fileName);
        final boolean hasFile = Files.exists(path);
        if (hasFile) {
            Files.deleteIfExists(path);
        }
        Files.createFile(path);
        @NotNull final Domain domain = new Domain();
        domainService = serviceLocator.getDomainService();
        domainService.save(domain);
        @NotNull final ObjectWriter writer = new ObjectMapper().writerWithDefaultPrettyPrinter();
        writer.writeValue(path.toFile(), domain);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public Role[] roles() {
        @NotNull final Role[] onlyAdmin = new Role[]{Role.ADMIN};
        return onlyAdmin;
    }

}
