package ru.teterin.tm.command.data.xml;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.entity.Domain;
import ru.teterin.tm.enumerated.Role;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public final class DataXmlFasterxmlLoadCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "data-xml-fx-load";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "The load of the subject area in the transport format XML with the use FasterXML.";
    }

    @Override
    public void execute() throws IOException {
        terminalService.print(Constant.DATA_XML_FX_LOAD);
        @NotNull final String fileName = Constant.DIRECTORY + "\\" + Constant.XML_FILE_NAME;
        @NotNull final Path path = Paths.get(fileName);
        final boolean noFile = !Files.exists(path);
        if (noFile) {
            throw new FileNotFoundException(Constant.FILE_NOT_FOUND);
        }
        @NotNull final XmlMapper mapper = new XmlMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        @NotNull final Domain domain = mapper.readValue(path.toFile(), Domain.class);
        stateService = serviceLocator.getStateService();
        stateService.setUserId(null);
        stateService.setLogin(null);
        stateService.setRole(null);
        stateService.setProjectId(null);
        taskService = serviceLocator.getTaskService();
        taskService.removeAll();
        projectService = serviceLocator.getProjectService();
        projectService.removeAll();
        userService = serviceLocator.getUserService();
        userService.removeAll();
        domainService = serviceLocator.getDomainService();
        domainService.load(domain);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public Role[] roles() {
        @NotNull final Role[] onlyAdmin = new Role[]{Role.ADMIN};
        return onlyAdmin;
    }

}
