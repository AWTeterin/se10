package ru.teterin.tm.command.data.binary;

import org.jetbrains.annotations.NotNull;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.entity.Domain;
import ru.teterin.tm.enumerated.Role;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public final class DataBinLoadCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "data-bin-load";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "The load of the subject area with the use of serialization.";
    }

    @Override
    public void execute() throws IOException {
        terminalService.print(Constant.DATA_BIN_LOAD);
        @NotNull final String fileName = Constant.DIRECTORY + "\\" + Constant.BIN_FILE_NAME;
        @NotNull final Path path = Paths.get(fileName);
        final boolean noFile = !Files.exists(path);
        if (noFile) {
            throw new FileNotFoundException(Constant.FILE_NOT_FOUND);
        }
        try (
            @NotNull final FileInputStream fileInputStream = new FileInputStream(path.toFile());
            @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        ) {
            @NotNull final Domain domain = (Domain) objectInputStream.readObject();
            stateService = serviceLocator.getStateService();
            stateService.setUserId(null);
            stateService.setLogin(null);
            stateService.setRole(null);
            stateService.setProjectId(null);
            taskService = serviceLocator.getTaskService();
            taskService.removeAll();
            projectService = serviceLocator.getProjectService();
            projectService.removeAll();
            userService = serviceLocator.getUserService();
            userService.removeAll();
            domainService = serviceLocator.getDomainService();
            domainService.load(domain);
        } catch (ClassNotFoundException | IOException e) {
            throw new IllegalArgumentException(Constant.INCORRECT_DATA_FILE);
        }
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public Role[] roles() {
        @NotNull final Role[] onlyAdmin = new Role[]{Role.ADMIN};
        return onlyAdmin;
    }

}
