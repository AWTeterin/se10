package ru.teterin.tm.command.data.json;

import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.eclipse.persistence.jaxb.JAXBContextProperties;
import org.jetbrains.annotations.NotNull;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.entity.Domain;
import ru.teterin.tm.enumerated.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public final class DataJsonJaxbLoadCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "data-json-jb-load";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "The load of the subject area in the transport format JSON with the use JAX-B.";
    }

    @Override
    public void execute() throws FileNotFoundException, JAXBException {
        terminalService.print(Constant.DATA_JSON_JB_LOAD);
        @NotNull final String fileName = Constant.DIRECTORY + "\\" + Constant.JSON_FILE_NAME;
        @NotNull final Path path = Paths.get(fileName);
        final boolean noFile = !Files.exists(path);
        if (noFile) {
            throw new FileNotFoundException(Constant.FILE_NOT_FOUND);
        }
        @NotNull final Map<String, Object> properties = new HashMap<String, Object>(2);
        properties.put(JAXBContextProperties.MEDIA_TYPE, "application/json");
        properties.put(JAXBContextProperties.JSON_INCLUDE_ROOT, true);
        @NotNull final Class[] clazz = new Class[]{Domain.class};
        @NotNull final JAXBContext jaxbContext = JAXBContextFactory.createContext(clazz, properties);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(path.toFile());
        stateService = serviceLocator.getStateService();
        stateService.setUserId(null);
        stateService.setLogin(null);
        stateService.setRole(null);
        stateService.setProjectId(null);
        taskService = serviceLocator.getTaskService();
        taskService.removeAll();
        projectService = serviceLocator.getProjectService();
        projectService.removeAll();
        userService = serviceLocator.getUserService();
        userService.removeAll();
        domainService = serviceLocator.getDomainService();
        domainService.load(domain);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public Role[] roles() {
        @NotNull final Role[] onlyAdmin = new Role[]{Role.ADMIN};
        return onlyAdmin;
    }

}
