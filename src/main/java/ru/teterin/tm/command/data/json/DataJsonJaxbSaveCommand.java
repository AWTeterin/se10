package ru.teterin.tm.command.data.json;

import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.eclipse.persistence.jaxb.JAXBContextProperties;
import org.jetbrains.annotations.NotNull;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.entity.Domain;
import ru.teterin.tm.enumerated.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public final class DataJsonJaxbSaveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "data-json-jb-save";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "The save of the subject area in the transport format JSON with the use JAX-B.";
    }

    @Override
    public void execute() throws IOException, JAXBException {
        terminalService.print(Constant.DATA_JSON_JB_SAVE);
        @NotNull final String fileName = Constant.DIRECTORY + "\\" + Constant.JSON_FILE_NAME;
        @NotNull final Path path = Paths.get(fileName);
        final boolean hasFile = Files.exists(path);
        if (hasFile) {
            Files.deleteIfExists(path);
        }
        Files.createFile(path);
        @NotNull final Domain domain = new Domain();
        domainService = serviceLocator.getDomainService();
        domainService.save(domain);
        @NotNull final Map<String, Object> properties = new HashMap<String, Object>(2);
        properties.put(JAXBContextProperties.MEDIA_TYPE, "application/json");
        properties.put(JAXBContextProperties.JSON_INCLUDE_ROOT, true);
        @NotNull final Class[] clazz = new Class[]{Domain.class};
        @NotNull final JAXBContext jaxbContext = JAXBContextFactory.createContext(clazz, properties);
        @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(domain, path.toFile());
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public Role[] roles() {
        @NotNull final Role[] onlyAdmin = new Role[]{Role.ADMIN};
        return onlyAdmin;
    }

}
