package ru.teterin.tm.command.data.binary;

import org.jetbrains.annotations.NotNull;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.entity.Domain;
import ru.teterin.tm.enumerated.Role;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public final class DataBinSaveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "data-bin-save";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "The save of the subject area with the use of serialization.";
    }

    @Override
    public void execute() throws IOException {
        terminalService.print(Constant.DATA_BIN_SAVE);
        @NotNull final String fileName = Constant.DIRECTORY + "\\" + Constant.BIN_FILE_NAME;
        @NotNull final Path path = Paths.get(fileName);
        final boolean hasFile = Files.exists(path);
        if (hasFile) {
            Files.deleteIfExists(path);
        }
        Files.createFile(path);
        @NotNull final Domain domain = new Domain();
        domainService = serviceLocator.getDomainService();
        domainService.save(domain);
        try (
            @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(path.toFile());
            @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        ) {
            objectOutputStream.writeObject(domain);
        }
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public Role[] roles() {
        @NotNull final Role[] onlyAdmin = new Role[]{Role.ADMIN};
        return onlyAdmin;
    }

}
