package ru.teterin.tm.command.data.xml;

import org.jetbrains.annotations.NotNull;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.entity.Domain;
import ru.teterin.tm.enumerated.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DataXmlJaxbLoadCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "data-xml-jb-load";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "The load of the subject area in the transport format XML with the use JAX-B.";
    }

    @Override
    public void execute() throws FileNotFoundException, JAXBException {
        terminalService.print(Constant.DATA_XML_JB_LOAD);
        @NotNull final String fileName = Constant.DIRECTORY + "\\" + Constant.XML_FILE_NAME;
        @NotNull final Path path = Paths.get(fileName);
        final boolean noFile = !Files.exists(path);
        if (noFile) {
            throw new FileNotFoundException(Constant.FILE_NOT_FOUND);
        }
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(path.toFile());
        stateService = serviceLocator.getStateService();
        stateService.setUserId(null);
        stateService.setLogin(null);
        stateService.setRole(null);
        stateService.setProjectId(null);
        taskService = serviceLocator.getTaskService();
        taskService.removeAll();
        projectService = serviceLocator.getProjectService();
        projectService.removeAll();
        userService = serviceLocator.getUserService();
        userService.removeAll();
        domainService = serviceLocator.getDomainService();
        domainService.load(domain);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public Role[] roles() {
        @NotNull final Role[] onlyAdmin = new Role[]{Role.ADMIN};
        return onlyAdmin;
    }

}
