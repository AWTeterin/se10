package ru.teterin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.entity.Project;

import java.util.Collection;

public final class ProjectListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all project.";
    }

    @Override
    public void execute() {
        terminalService.print(Constant.PROJECT_LIST);
        stateService = serviceLocator.getStateService();
        @Nullable final String userId = stateService.getUserId();
        projectService = serviceLocator.getProjectService();
        @NotNull final Collection<Project> projects = projectService.findAll(userId);
        terminalService.printCollection(projects);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

}
