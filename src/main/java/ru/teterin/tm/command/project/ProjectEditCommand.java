package ru.teterin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.entity.Project;
import ru.teterin.tm.enumerated.Status;

import java.util.Date;

public final class ProjectEditCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-edit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Choose project by id and edit it.";
    }

    @Override
    public void execute() {
        terminalService.print(Constant.PROJECT_EDIT);
        terminalService.print(Constant.ENTER_PROJECT_ID);
        @Nullable final String id = terminalService.readString();
        stateService = serviceLocator.getStateService();
        @Nullable final String userId = stateService.getUserId();
        projectService = serviceLocator.getProjectService();
        @NotNull final Project project = projectService.findOne(userId, id);
        terminalService.print(project);
        @NotNull final Project result = terminalService.readProject();
        @NotNull final Date dateCreate = project.getDateCreate();
        result.setDateCreate(dateCreate);
        if (userId != null) {
            result.setUserId(userId);
        }
        if (id != null) {
            result.setId(id);
        }
        terminalService.print(Constant.ENTER_STATUS);
        @Nullable final String strStatus = terminalService.readString();
        if (Constant.READY.equals(strStatus)) {
            result.setStatus(Status.READY);
        }
        if (Constant.IN_PROGRESS.equals(strStatus)) {
            result.setStatus(Status.IN_PROGRESS);
        }
        if (Constant.PLANNED.equals(strStatus)) {
            result.setStatus(Status.PLANNED);
        }
        projectService.checkProject(userId, result);
        projectService.merge(userId, result);
        stateService.setProjectId(id);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

}
