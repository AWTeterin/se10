package ru.teterin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;

public final class ProjectRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-remove";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove project by ID.";
    }

    @Override
    public void execute() {
        terminalService.print(Constant.PROJECT_REMOVE);
        terminalService.print(Constant.ENTER_PROJECT_ID);
        stateService = serviceLocator.getStateService();
        @Nullable final String userId = stateService.getUserId();
        @Nullable final String projectId = terminalService.readString();
        projectService = serviceLocator.getProjectService();
        projectService.remove(userId, projectId);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

}
