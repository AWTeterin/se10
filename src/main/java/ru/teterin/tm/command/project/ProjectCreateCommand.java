package ru.teterin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.entity.Project;

public final class ProjectCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create new Project.";
    }

    @Override
    public void execute() {
        terminalService.print(Constant.PROJECT_CREATE);
        @NotNull final Project project = terminalService.readProject();
        stateService = serviceLocator.getStateService();
        @Nullable final String userId = stateService.getUserId();
        if (userId != null) {
            project.setUserId(userId);
        }
        projectService = serviceLocator.getProjectService();
        projectService.checkProject(userId, project);
        projectService.persist(userId, project);
        @NotNull final String projectId = project.getId();
        stateService.setProjectId(projectId);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

}
