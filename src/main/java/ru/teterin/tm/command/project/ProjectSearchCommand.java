package ru.teterin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.entity.Project;

import java.util.Collection;

public final class ProjectSearchCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-search";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Search for a string in the project name or description.";
    }

    @Override
    public void execute() {
        terminalService.print(Constant.PROJECT_SEARCH);
        terminalService.print(Constant.ENTER_SEARCH_STRING);
        @Nullable final String searchString = terminalService.readString();
        stateService = serviceLocator.getStateService();
        @Nullable final String userId = stateService.getUserId();
        projectService = serviceLocator.getProjectService();
        @NotNull final Collection<Project> projects = projectService.searchByString(userId, searchString);
        terminalService.printCollection(projects);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

}
