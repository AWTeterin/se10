package ru.teterin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.entity.Task;

import java.util.Collection;

public final class ProjectTasksCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-tasks";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all tasks for this project.";
    }

    @Override
    public void execute() {
        terminalService.print(Constant.PROJECT_TASKS);
        terminalService.print(Constant.ENTER_PROJECT_ID);
        @Nullable final String id = terminalService.readString();
        stateService = serviceLocator.getStateService();
        @Nullable final String userId = stateService.getUserId();
        projectService = serviceLocator.getProjectService();
        @NotNull final Collection<Task> tasks = projectService.findAllTaskByProjectId(userId, id);
        terminalService.printCollection(tasks);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

}
