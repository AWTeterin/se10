package ru.teterin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.enumerated.Role;

public final class UserShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-show";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show user data.";
    }

    @Override
    public void execute() {
        terminalService.print(Constant.USER_SHOW);
        stateService = serviceLocator.getStateService();
        @Nullable final Role role = stateService.getRole();
        @NotNull final String login = Constant.USER + stateService.getLogin();
        if (role == null) {
            throw new IllegalArgumentException(Constant.NO_ROLE);
        }
        @NotNull String roleInfo = Constant.ROLE + role.displayName();
        terminalService.print(login);
        terminalService.print(roleInfo);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

}
