package ru.teterin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.entity.User;
import ru.teterin.tm.enumerated.Role;

public final class UserCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-reg";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Registration new user.";
    }

    @Override
    public void execute() {
        terminalService.print(Constant.USER_REGISTRATION);
        @NotNull User user = new User();
        terminalService.print(Constant.ENTER_LOGIN);
        @Nullable final String login = terminalService.readString();
        user.setName(login);
        terminalService.print(Constant.ENTER_PASSWORD);
        @Nullable String password = terminalService.readString();
        user.setPassword(password);
        userService = serviceLocator.getUserService();
        user = userService.create(user);
        @NotNull final String userId = user.getId();
        userService.persist(userId, user);
        stateService = serviceLocator.getStateService();
        stateService.setProjectId(null);
        stateService.setUserId(userId);
        stateService.setLogin(login);
        @Nullable final Role role = user.getRole();
        stateService.setRole(role);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return false;
    }

}
