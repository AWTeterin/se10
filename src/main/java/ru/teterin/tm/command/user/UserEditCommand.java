package ru.teterin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;

public final class UserEditCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-edit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Edit user login.";
    }

    @Override
    public void execute() {
        terminalService.print(Constant.USER_EDIT);
        terminalService.print(Constant.ENTER_NEW_LOGIN);
        @Nullable final String newLogin = terminalService.readString();
        stateService = serviceLocator.getStateService();
        @Nullable final String oldLogin = stateService.getLogin();
        userService = serviceLocator.getUserService();
        userService.editUser(oldLogin, newLogin);
        stateService.setLogin(newLogin);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

}
