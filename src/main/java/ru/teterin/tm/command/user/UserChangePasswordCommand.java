package ru.teterin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;

public final class UserChangePasswordCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-password";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change user password.";
    }

    @Override
    public void execute() {
        terminalService.print(Constant.CHANGE_PASSWORD);
        terminalService.print(Constant.ENTER_PASSWORD);
        @Nullable final String oldPassword = terminalService.readString();
        terminalService.print(Constant.ENTER_NEW_PASSWORD);
        @Nullable final String newPassword = terminalService.readString();
        stateService = serviceLocator.getStateService();
        @Nullable final String login = stateService.getLogin();
        userService = serviceLocator.getUserService();
        userService.changePassword(login, oldPassword, newPassword);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

}
