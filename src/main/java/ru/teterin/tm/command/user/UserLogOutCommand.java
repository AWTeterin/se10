package ru.teterin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;

public final class UserLogOutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-logout";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Log out to the task manager.";
    }

    @Override
    public void execute() {
        terminalService.print(Constant.USER_LOGOUT);
        stateService = serviceLocator.getStateService();
        @Nullable final String login = stateService.getLogin();
        @NotNull final String byeMessage = Constant.BYE + login;
        terminalService.print(byeMessage);
        stateService.setUserId(null);
        stateService.setLogin(null);
        stateService.setRole(null);
        stateService.setProjectId(null);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

}
