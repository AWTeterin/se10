package ru.teterin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;

public final class TaskRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-remove";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove task by ID.";
    }

    @Override
    public void execute() {
        terminalService.print(Constant.TASK_REMOVE);
        terminalService.print(Constant.ENTER_TASK_ID);
        stateService = serviceLocator.getStateService();
        @Nullable final String userId = stateService.getUserId();
        @Nullable final String id = terminalService.readString();
        taskService = serviceLocator.getTaskService();
        taskService.remove(userId, id);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

}
