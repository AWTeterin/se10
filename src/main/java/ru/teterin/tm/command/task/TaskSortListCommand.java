package ru.teterin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.entity.Task;

import java.util.Collection;

public final class TaskSortListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-sort";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Find all tasks and sort by field and type sort. Show result.";
    }

    @Override
    public void execute() {
        terminalService.print(Constant.TASK_SORT);
        terminalService.print(Constant.ENTER_SORT_FIELD);
        @Nullable final String sortOptions = terminalService.readString();
        stateService = serviceLocator.getStateService();
        @Nullable final String userId = stateService.getUserId();
        taskService = serviceLocator.getTaskService();
        @NotNull final Collection<Task> tasks = taskService.findAndSortAll(userId, sortOptions);
        terminalService.printCollection(tasks);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

}
