package ru.teterin.tm.constant;

import org.jetbrains.annotations.NotNull;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;

public final class Constant {

    @NotNull
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");

    @NotNull
    public static final String MD5 = "MD5";

    @NotNull
    public static final String ROOT_PACKAGE = "ru.teterin.tm";

    @NotNull
    public static final Path DIRECTORY = Paths.get(System.getProperty("user.dir"));

    @NotNull
    public static final String BIN_FILE_NAME = "data.bin";

    @NotNull
    public static final String JSON_FILE_NAME = "data.json";

    @NotNull
    public static final String XML_FILE_NAME = "data.xml";

    @NotNull
    public static final String START_MESSAGE = "*** WELCOME TO TASK MANAGER ***";

    @NotNull
    public static final String CORRECT_EXECUTION = "[OK]";

    @NotNull
    public static final String ENTER_PROJECT_ID = "ENTER PROJECT ID:";

    @NotNull
    public static final String ENTER_TASK_ID = "ENTER TASK ID:";

    @NotNull
    public static final String ENTER_NAME = "ENTER NAME:";

    @NotNull
    public static final String ENTER_DESCRIPTION = "ENTER DESCRIPTION:";

    @NotNull
    public static final String ENTER_START_DATE = "ENTER START DATE:";

    @NotNull
    public static final String ENTER_END_DATE = "ENTER END DATE:";

    @NotNull
    public static final String ENTER_LOGIN = "ENTER LOGIN:";

    @NotNull
    public static final String ENTER_NEW_LOGIN = "ENTER NEW LOGIN:";

    @NotNull
    public static final String ENTER_PASSWORD = "ENTER PASSWORD:";

    @NotNull
    public static final String ENTER_NEW_PASSWORD = "ENTER NEW PASSWORD:";

    @NotNull
    public static final String ENTER_STATUS = "ENTER STATUS:\n" +
        "VALID VALUE: ready, in-progress.";

    @NotNull
    public static final String ENTER_SEARCH_STRING = "ENTER THE STRING TO SEARCH:";

    @NotNull
    public static final String ENTER_SORT_FIELD = "SORT FIELD OPTION:\n" +
        "    1 - SORT BY CREATE DATE,\n    2 - SORT BY START DATE,\n    3 -SORT BY END DATE,\n    4 - SORT BY STATUS. \n" +
        "Default value: 1 - SORT BY CREATE DATE.\n" +
        "SORT TYPE OPTION:\n" +
        "    1 - ASCENDING SORT, \n    2 - DESCENDING SORT. \n" +
        "Default value: 1 asc. \n" +
        "ENTER WITHOUT SPACES NUMBER SORT OPTION AND CLICK ENTER: ";

    @NotNull
    public static final String PROJECT_CLEAR = "[PROJECTS CLEAR]";

    @NotNull
    public static final String PROJECT_CREATE = "[PROJECT CREATE]";

    @NotNull
    public static final String PROJECT_EDIT = "[PROJECT EDIT]";

    @NotNull
    public static final String PROJECT_LIST = "[PROJECT LIST]";

    @NotNull
    public static final String PROJECT_REMOVE = "[PROJECT REMOVE]";

    @NotNull
    public static final String PROJECT_TASKS = "[PROJECT TASK]";

    @NotNull
    public static final String PROJECT_SORT = "[PROJECT SORT]";

    @NotNull
    public static final String PROJECT_SEARCH = "[PROJECT SEARCH]";

    @NotNull
    public static final String ABOUT = "[ABOUT]";

    @NotNull
    public static final String TASK_CLEAR = "[TASK CLEAR]";

    @NotNull
    public static final String TASK_CREATE = "[TASK CREATE]";

    @NotNull
    public static final String TASK_EDIT = "[TASK EDIT]";

    @NotNull
    public static final String TASK_LINK = "[TASK_LINK]";

    @NotNull
    public static final String TASK_LIST = "[TASK LIST]";

    @NotNull
    public static final String TASK_REMOVE = "[TASK REMOVE]";

    @NotNull
    public static final String TASK_SORT = "[TASK SORT]";

    @NotNull
    public static final String TASK_SEARCH = "[TASK SEARCH]";

    @NotNull
    public static final String CHANGE_PASSWORD = "[CHANGE PASSWORD]";

    @NotNull
    public static final String USER_REGISTRATION = "[USER REGISTRATION]";

    @NotNull
    public static final String USER_EDIT = "[USER EDIT]";

    @NotNull
    public static final String USER_LOGIN = "[USER LOGIN]";

    @NotNull
    public static final String USER_LOGOUT = "[USER LOGOUT]";

    @NotNull
    public static final String USER_SHOW = "[USER SHOW]";

    @NotNull
    public static final String DATA_BIN_SAVE = "[DATA BINARY SAVE]";

    @NotNull
    public static final String DATA_BIN_LOAD = "[DATA BINARY LOAD]";

    @NotNull
    public static final String DATA_JSON_JB_SAVE = "[DATA JSON JAXB SAVE]";

    @NotNull
    public static final String DATA_JSON_JB_LOAD = "[DATA JSON JAXB LOAD]";

    @NotNull
    public static final String DATA_JSON_FX_SAVE = "[DATA JSON FASTERXML SAVE]";

    @NotNull
    public static final String DATA_JSON_FX_LOAD = "[DATA JSON FASTERXML LOAD]";

    @NotNull
    public static final String DATA_XML_JB_SAVE = "[DATA XML JAXB SAVE]";

    @NotNull
    public static final String DATA_XML_JB_LOAD = "[DATA XML JAXB LOAD]";

    @NotNull
    public static final String DATA_XML_FX_SAVE = "[DATA XML FASTERXML SAVE]";

    @NotNull
    public static final String DATA_XML_FX_LOAD = "[DATA XML FASTERXML LOAD]";

    @NotNull
    public static final String INVALID_COMMAND = "AN INVALID COMMAND WAS SUBMITTED IN THE COMMAND LIST!";

    @NotNull
    public static final String INCORRECT_COMMAND = "THE COMMAND IS NOT AVAILABLE!";

    @NotNull
    public static final String INCORRECT_PASSWORD = "INCORRECT PASSWORD! TRY A DIFFERENT PASSWORD!";

    @NotNull
    public static final String INCORRECT_ID = "INCORRECT FORMAT ID! TRY THIS FORMAT: 00000000-0000-0000-0000-000000000000";

    @NotNull
    public static final String INCORRECT_DATE = "INCORRECT FORMAT DATE! TRY THIS FORMAT: 01.01.2020";

    @NotNull
    public static final String INCORRECT_DATA_FILE = "INCORRECT FILE! UNABLE TO READ DATA FROM THE FILE!";

    @NotNull
    public static final String USER_NOT_FOUND = "USER NOT FOUND! TRY A DIFFERENT LOGIN!";

    @NotNull
    public static final String OBJECT_EXIST = "OBJECT ALREADY EXIST! TRY CREATING A DIFFERENT OBJECT!";

    @NotNull
    public static final String OBJECT_NOT_FOUND = "OBJECT NOT FOUND!";

    @NotNull
    public static final String FILE_NOT_FOUND = "FILE NOT FOUND!";

    @NotNull
    public static final String NO_COMMAND = "THE COMMAND CANNOT BE EMPTY!";

    @NotNull
    public static final String NO_PROJECT = "YOU CAN'T CREATE AN ISSUE WITHOUT A PROJECT! FIRST, CREATE A PROJECT!";

    @NotNull
    public static final String NO_TASK = "THE TASK CANNOT BE EMPTY";

    @NotNull
    public static final String NO_DOMAIN = "THE DOMAIN CANNOT BE EMPTY";

    @NotNull
    public static final String NO_ROLE = "THE ROLE CANNOT BE EMPTY!";

    @NotNull
    public static final String NO_PASSWORD = "THE PASSWORD CANNOT BE EMPTY!";

    @NotNull
    public static final String EMPTY_ID = "ID CAN'T BE EMPTY!";

    @NotNull
    public static final String EMPTY_DATE = "DATE CAN'T BE EMPTY!";

    @NotNull
    public static final String EMPTY_OBJECT = "OBJECT OR ITS FIELDS CANNOT BE EMPTY!";

    @NotNull
    public static final String ERROR_OBJECT_ACCESS = "OBJECT CANNOT BE ACCESSED!";

    @NotNull
    public static final String CREATED_BY = "Created-by";

    @NotNull
    public static final String BUILD_JDK = "Build-Jdk-Spec";

    @NotNull
    public static final String BUILD_NUMBER = "buildNumber";

    @NotNull
    public static final String DEVELOPER = "developer";

    @NotNull
    public static final String HELLO = "HELLO ";

    @NotNull
    public static final String BYE = "BYE BYE ";

    @NotNull
    public static final String USER = "USER: ";

    @NotNull
    public static final String ROLE = "ROLE: ";

    @NotNull
    public static final String CREATE_DATE = "CREATE DATE";

    @NotNull
    public static final String START_DATE = "START DATE";

    @NotNull
    public static final String END_DATE = "END DATE";

    @NotNull
    public static final String STATUS = "STATUS";

    @NotNull
    public static final String ASC = "ASC";

    @NotNull
    public static final String DESC = "DESC";

    @NotNull
    public static final String PLANNED = "planned";

    @NotNull
    public static final String IN_PROGRESS = "in progress";

    @NotNull
    public static final String READY = "ready";

    @NotNull
    public static final String CREATE_ASC_SORT = "11";

    @NotNull
    public static final String CREATE_DESC_SORT = "12";

    @NotNull
    public static final String START_DATE_ASC_SORT = "21";

    @NotNull
    public static final String START_DATE_DESC_SORT = "22";

    @NotNull
    public static final String END_DATE_ASC_SORT = "31";

    @NotNull
    public static final String END_DATE_DESC_SORT = "32";

    @NotNull
    public static final String STATUS_ASC_SORT = "41";

    @NotNull
    public static final String STATUS_DESC_SORT = "42";

    public static final int WITHOUT_ERRORS = 0;

}
