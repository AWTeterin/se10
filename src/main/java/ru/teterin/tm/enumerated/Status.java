package ru.teterin.tm.enumerated;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.Nullable;

@AllArgsConstructor
public enum Status {

    PLANNED("PLANNED"),
    IN_PROGRESS("IN PROGRESS"),
    READY("READY");

    @Nullable
    private final String name;

    @Nullable
    public String displayName() {
        return name;
    }

}
